#include <cstdio>
#include <cstring>
#include <algorithm>
#define MAXN 100
using namespace std;

const int inf = 1e5;

int dp[MAXN][MAXN];
int used[MAXN];
int n;

/**
  Initializes graph.
*/
void initGraph(void) {
  for (int i = 0; i < MAXN; i++) {
    for (int j = 0; j < MAXN; j++) {
      dp[i][j] = inf;
    }
  }
  memset(used, 0, sizeof (used));
}

/**
  Reads edges for current test case.
*/
bool readEdges(void) {
  bool moreInput = false;
  while(true) {
    int a, b; scanf ("%d%d", &a, &b);
    if (a == 0) {
      break;
    }
    moreInput = true;
    n = max(n, max(a, b));
    a--; b--;
    used[a] = used[b] = 1;
    dp[a][b] = 1;
  }
  return moreInput;
}

/**
  Runs Floyd-Warshall algorithm on graph.
*/
void calcAllPaths() {
  for (int k = 0; k < n; k++) {
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
          // is some of those nodes is not used, their value used[x] is 0, so
          // multiplication gives 0
        if (used[k] * used[i] * used[j] != 0) {
          dp[i][j] = min(dp[i][j], dp[i][k] + dp[k][j]);
        }
      }
    }
  }
}

/**
  Calculates the number of nodes in graph (not all nodes from 1 to n are used).
*/
int calcNumberOfNodes(void) {
  int numberOfNodes = 0;
  for (int i = 0; i < n; i++) {
    if (used[i] == 1) {
      numberOfNodes++;
    }
  }
  return numberOfNodes;
}

/**
  Calculates the number of paths in graph.
*/
int calcNumberOfPaths(void) {
  int numberOfPaths = 0;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      // again, if some node x is not used it's value used[x] is 0
      // we also don't count paths to itself
      if (used[i] * used[j] != 0 && i != j) {
        numberOfPaths += dp[i][j];
      }
    }
  }
  return numberOfPaths;
}

/**
  Print dp[i][j] to standard output.
*/
void outputMatrix(void) {
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      printf ("%d ", dp[i][j]);
    } printf ("\n");
  }
}

int main(void) {
  int test = 0;
  while(true) {
    test++;
    initGraph();
    bool moreInput = readEdges();
    if (moreInput == false) {
      break;
    }

    //printf ("before: \n");
    //outputMatrix();

    calcAllPaths();
    int numberOfNodes = calcNumberOfNodes();
    int numberOfPaths = calcNumberOfPaths();
    //printf ("paths: %d, nodes: %d\n", numberOfPaths, numberOfNodes);
    printf ("Case %d: average length between pages = %.3f clicks\n",
            test,
            numberOfPaths / ((double)numberOfNodes * (numberOfNodes - 1))
    );

    //printf ("\nafter: \n");
    //outputMatrix();
  }

  return 0;
}
